import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/component/loading.dart';
import 'package:pasar_desa_sambungrejo_flutter/config.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/provider/cart_controller.dart';
import 'package:pasar_desa_sambungrejo_flutter/helper/app_alert.dart';
import 'package:pasar_desa_sambungrejo_flutter/helper/auth_helper.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/app_exxception.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/product_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/confirmation_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/product_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/transaction/transaction_history_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/update_user_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isLoading = true;
  List<ProductModel> products = [];
  @override
  void initState() {
    super.initState();
    fetchProduct();
  }

  fetchProduct() async {
    try {
      List<dynamic> response = await Request.get('/barang');
      print(response);
      setState(() {
        products = response.map((e) => ProductModel.fromJson(e)).toList();
        isLoading = false;
      });
    } on AppException catch (err) {
      AppAlert.showErrorFlash(message: err.message);
    }
  }

  addToCart(ProductModel product) {
    CartProvider.to.addProduct(product);
  }

  handleClick(String value) {
    switch (value) {
      case 'Riwayat Transaksi':
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => TransactionHistoryPage(),
          ),
        );
        break;
      case 'Logout':
        AuthHelper.logout(context);
        break;
      case 'Update Data User':
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => UpdateUserPage()));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary,
        title: Text("Pasar Desa Sambung Rejo"),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {
                'Riwayat Transaksi',
                'Call Center',
                'SMS Center',
                'Update Data User',
                'Logout'
              }.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: isLoading
          ? Center(child: Loading())
          : Stack(
              children: [
                Container(
                  padding:
                      EdgeInsets.only(top: 15, right: 20, left: 20, bottom: 50),
                  child: GridView.count(
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    crossAxisCount: 2,
                    childAspectRatio: 1 / 1.58,
                    children: products
                        .map((ProductModel product) => _productItem(product))
                        .toList(),
                  ),
                ),
                GetBuilder<CartProvider>(
                  builder: (provider) => Positioned(
                    bottom: 20,
                    left: 20,
                    right: 20,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => ConfirmationPage(
                              transactionAmount: provider.total,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(25),
                          ),
                          color: AppColor.primary,
                        ),
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 10),
                        child: Text(
                          "Total = ${provider.total}",
                          style: TextStyle(fontSize: 25, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
    );
  }

  Widget _productItem(ProductModel product) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              addToCart(product);
            },
            child: Container(
              height: 200,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: Image.network(
                          "${AppConfig.gateway}/images/${product.kdBrg}.jpg")
                      .image,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => ProductPage(product: product),
                ),
              );
            },
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${product.nmBrg}",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text("${product.harga}", style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
