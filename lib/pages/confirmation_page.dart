import 'package:flutter/material.dart';
import 'package:load/load.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/rclient.dart';
import 'package:pasar_desa_sambungrejo_flutter/component/selector.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/provider/cart_controller.dart';
import 'package:pasar_desa_sambungrejo_flutter/helper/app_alert.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/app_exxception.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/product_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/transaction_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/transaction/transaction_detail_page.dart';

class ConfirmationPage extends StatefulWidget {
  const ConfirmationPage({Key? key, required this.transactionAmount})
      : super(key: key);
  final int transactionAmount;
  @override
  _ConfirmationPageState createState() => _ConfirmationPageState();
}

class _ConfirmationPageState extends State<ConfirmationPage> {
  List<dynamic> provinces = [];
  List<dynamic> toCities = [];
  Map<String, dynamic>? selectedToProvince;
  Map<String, dynamic>? selectedToCity;
  Map<String, dynamic>? selectedCourier;
  TextEditingController weightController = TextEditingController();
  List<dynamic> couriers = [];
  int ongkir = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchProvinces();
  }

  fetchProvinces() async {
    dynamic response = await OngkirRequest.get("/province");
    setState(() {
      this.provinces = response['rajaongkir']['results'];
    });
  }

  void fetchToCities() async {
    showLoadingDialog(tapDismiss: true);
    dynamic response = await OngkirRequest.get(
        "/city?province=${selectedToProvince!['province_id']}");
    print(response);
    setState(() {
      this.toCities = response['rajaongkir']['results'];
    });
    hideLoadingDialog();
  }

  void getOngkir() async {
    showLoadingDialog(tapDismiss: false);
    var payload = {
      "origin": 399,
      "destination": selectedToCity!['city_id'],
      "courier": selectedCourier!['courier'],
      "weight": 100
    };
    print(payload);
    dynamic response = await OngkirRequest.post("/cost", data: payload);
    response = response['rajaongkir'];
    print(response);
    print(response['results'][0]['costs'][0]['cost'][0]['value']);
    setState(() {
      this.ongkir = response['results'][0]['costs'][0]['cost'][0]['value'];
    });
    hideLoadingDialog();
  }

  void makeTransaction() async {
    try {
      showLoadingDialog(tapDismiss: false);
      dynamic payload = {
        'total_pembelian': CartProvider.to.total,
        'ongkir': this.ongkir,
        'total': CartProvider.to.total + this.ongkir,
        'kota': this.selectedToCity!['city_name'],
        'provinsi': this.selectedToProvince!['province'],
        'products': CartProvider.to.products
            .map((ProductModel product) => product.toJson())
            .toList()
      };
      dynamic response = await Request.post('/transaction', data: payload);
      TransactionModel transaction = TransactionModel.fromJson(response);
      CartProvider.to.reset();
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => TransactionDetailPage(transaction: transaction,)));
      hideLoadingDialog();
    } on AppException catch (err) {
      AppAlert.showErrorFlash(title: "Terjadi Error", message: err.message);
      hideLoadingDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Konfirmasi Pesanan"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            const Text(
              "Tujuan Pengiriman",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 10,
            ),
            Selector(
              label: "Provinsi",
              items: provinces,
              labelKey: 'province',
              onSelect: (item) {
                setState(() {
                  selectedToProvince = item;
                });
                fetchToCities();
              },
            ),
            const SizedBox(
              height: 5,
            ),
            Selector(
              label: "Kota",
              items: toCities,
              labelKey: 'city_name',
              onSelect: (city) {
                setState(() {
                  selectedToCity = city;
                });
              },
              itemWidget: (item) {
                return Column(
                  children: [
                    ListTile(
                      title: Text("${item['type']} ${item['city_name']}"),
                    ),
                    const Divider(
                      height: 2,
                    )
                  ],
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            const SizedBox(
              height: 5,
            ),
            Selector(
              label: "Pilih Kurir",
              items: const [
                {"courier": "pos"},
                {"courier": "tiki"},
                {"courier": "jne"},
              ],
              labelKey: 'courier',
              onSelect: (courier) {
                setState(() {
                  selectedCourier = courier;
                  getOngkir();
                });
              },
            ),
            const SizedBox(
              height: 25,
            ),
            Text(
              "Total Pembelian : ${widget.transactionAmount}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            Text(
              "Ongkir : ${ongkir}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            Text(
              "Total Pesanan : ${widget.transactionAmount + ongkir}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 25,
            ),
            TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Theme.of(context).primaryColor),
              ),
              onPressed: () {
                makeTransaction();
              },
              child: const Text(
                "Lanjutkan ke Pembayaran",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
