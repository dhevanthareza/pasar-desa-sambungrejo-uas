class TransactionModel {
  int? id;
  String? tanggal;
  int? totalPembelian;
  int? ongkir;
  int? total;
  int? statusPembayaran;
  String? kota;
  String? provinsi;
  int? idUser;

  TransactionModel(
      {this.id,
      this.tanggal,
      this.totalPembelian,
      this.ongkir,
      this.total,
      this.statusPembayaran,
      this.kota,
      this.provinsi,
      this.idUser});

  TransactionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tanggal = json['tanggal'];
    totalPembelian = json['total_pembelian'];
    ongkir = json['ongkir'];
    total = json['total'];
    statusPembayaran = json['status_pembayaran'];
    kota = json['kota'];
    provinsi = json['provinsi'];
    idUser = json['id_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tanggal'] = this.tanggal;
    data['total_pembelian'] = this.totalPembelian;
    data['ongkir'] = this.ongkir;
    data['total'] = this.total;
    data['status_pembayaran'] = this.statusPembayaran;
    data['kota'] = this.kota;
    data['provinsi'] = this.provinsi;
    data['id_user'] = this.idUser;
    return data;
  }
}