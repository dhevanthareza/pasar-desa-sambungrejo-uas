import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:load/load.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/sharedpref/auth_preferences.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/app_exxception.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/user_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/auth/register_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextStyle textFieldTextStyle = const TextStyle(fontSize: 20);
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  
  void login() async {
    try {
      showLoadingDialog(tapDismiss: false);
      dynamic response = await Request.post('/auth/login', data: {
        "user_id": usernameController.text,
        "password": passwordController.text
      });
      UserModel user = UserModel.fromJson(response);
      await AuthPreferences.saveAuthPref(user);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => HomePage(),
        ),
      );
      hideLoadingDialog();
    } on AppException catch (err) {
      hideLoadingDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Image.asset(
              "assets/images/login.png",
              width: 350,
            ),
            const SizedBox(height: 10),
            Row(
              children: const [
                Text(
                  "Login",
                  style: TextStyle(fontSize: 40),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: usernameController,
                    style: textFieldTextStyle,
                    decoration: InputDecoration(
                      label: const Text("Username"),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 2),
                      labelStyle: textFieldTextStyle,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.lock,
                  color: Colors.grey,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: passwordController,
                    style: textFieldTextStyle,
                    decoration: InputDecoration(
                      label: const Text("Password"),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 2),
                      labelStyle: textFieldTextStyle,
                    ),
                    obscureText: true,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            TextButton(
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.all(5)),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 0)),
                  backgroundColor: MaterialStateProperty.all(AppColor.primary)),
              onPressed: () {
                login();
              },
              child: const Text(
                "Login",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    width: double.infinity,
                    height: 1.5,
                    color: Color(0xFFefefef),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                const Text(
                  "OR",
                  style: TextStyle(color: Colors.grey, fontSize: 20),
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    height: 1.5,
                    color: Color(0xFFefefef),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.all(5)),
                minimumSize:
                    MaterialStateProperty.all(Size(double.infinity, 0)),
                backgroundColor: MaterialStateProperty.all(Color(0xFFefefef)),
              ),
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/google.png",
                    height: 20,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  const Text(
                    "Login with Google",
                    style: TextStyle(fontSize: 20, color: Colors.black87),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            RichText(
                text: TextSpan(
              text: "Belum memeiliki akun?",
              style: const TextStyle(
                fontSize: 11,
                color: Colors.black54,
                fontWeight: FontWeight.w500,
              ),
              children: [
                TextSpan(
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => RegisterPage(),
                        ),
                      );
                    },
                  text: " Daftar",
                  style: TextStyle(
                      color: AppColor.primary, fontWeight: FontWeight.w500),
                ),
              ],
            ))
          ]),
        ),
      ),
    );
  }
}
