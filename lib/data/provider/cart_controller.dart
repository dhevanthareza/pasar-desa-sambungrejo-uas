import 'package:get/get.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/product_model.dart';

class CartProvider extends GetxController {
  List<ProductModel> products = [];
  int total = 0;
  
  static CartProvider get to => Get.find();

  addProduct(ProductModel product) {
    products.add(product);
    total += product.harga!;
    update();
  }

  reset() {
    products = [];
    total = 0;
  }
}