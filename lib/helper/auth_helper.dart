import 'package:flutter/material.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/auth/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthHelper {
  static Future<dynamic> logout(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (_) => LoginPage()), (route) => false);
  }
}
