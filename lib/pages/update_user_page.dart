import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:load/load.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/sharedpref/auth_preferences.dart';
import 'package:pasar_desa_sambungrejo_flutter/helper/app_alert.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/app_exxception.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/user_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/auth/login_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/home_page.dart';

class UpdateUserPage extends StatefulWidget {
  const UpdateUserPage({Key? key}) : super(key: key);

  @override
  _UpdateUserPageState createState() => _UpdateUserPageState();
}

class _UpdateUserPageState extends State<UpdateUserPage> {
  final TextStyle textFieldTextStyle = const TextStyle(fontSize: 20);
  final usernameController = TextEditingController();
  final nameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    initializeUser();
  }

  void initializeUser() async {
    UserModel? user = await AuthPreferences.getUser();
    setState(() {
      usernameController.text = user!.userId!;
      nameController.text = user.name!;
    });
  }

  void update() async {
    try {
      showLoadingDialog(tapDismiss: false);
      Map<String, dynamic> payload = {
        "username": usernameController.text,
        "name": nameController.text
      };
      if(passwordController.text != null && passwordController.text != "") {
        payload['password'] = passwordController.text;
      }
      dynamic response = await Request.post('/auth/update', data: payload);
      UserModel user = UserModel.fromJson(response);
      await AuthPreferences.saveAuthPref(user);
      Navigator.pop(context);
      hideLoadingDialog();
      AppAlert.showSuccessFlash(
          title: "Aksi Berhasil", message: "Berhasil mengubah data user");
    } on AppException catch (err) {
      hideLoadingDialog();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
        child: SingleChildScrollView(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Image.asset(
              "assets/images/update_user.png",
              width: 350,
            ),
            const SizedBox(height: 5),
            Row(
              children: const [
                Text(
                  "Perbarui data user",
                  style: TextStyle(fontSize: 40),
                ),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: usernameController,
                    style: textFieldTextStyle,
                    decoration: InputDecoration(
                      label: const Text("Username"),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 2),
                      labelStyle: textFieldTextStyle,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: nameController,
                    style: textFieldTextStyle,
                    decoration: InputDecoration(
                      label: const Text("Nama Lengkap"),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 2),
                      labelStyle: textFieldTextStyle,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.lock,
                  color: Colors.grey,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextField(
                    controller: passwordController,
                    style: textFieldTextStyle,
                    decoration: InputDecoration(
                      label: const Text("Password"),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 2),
                      labelStyle: textFieldTextStyle,
                    ),
                    obscureText: true,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 25,
            ),
            TextButton(
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.all(5)),
                  minimumSize:
                      MaterialStateProperty.all(Size(double.infinity, 0)),
                  backgroundColor: MaterialStateProperty.all(AppColor.primary)),
              onPressed: () {
                update();
              },
              child: const Text(
                "Update data user",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ]),
        ),
      ),
    );
  }
}
