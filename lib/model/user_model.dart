class UserModel {
  int? id;
  String? userId;
  String? name;
  int? hakAkses;
  String? token;

  UserModel({this.id, this.userId, this.name, this.hakAkses, this.token});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    hakAkses = json['hak_akses'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['hak_akses'] = this.hakAkses;
    data['token'] = this.token;
    return data;
  }
}