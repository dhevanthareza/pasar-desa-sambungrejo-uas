import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppAlert {
  static showErrorFlash({
    title,
    message,
    context,
  }) {
    Get.snackbar(
      title,
      message,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      duration: const Duration(seconds: 2),
    );
  }

  static showWarningFlash({
    title,
    message,
    context,
  }) {
    Get.snackbar(
      title,
      message,
      backgroundColor: Theme.of(context).accentColor,
      colorText: Colors.white,
      duration: const Duration(seconds: 5),
    );
  }

  static showSuccessFlash({
    title,
    message,
    context,
  }) {
    Get.snackbar(
      title,
      message,
      backgroundColor: Colors.green,
      colorText: Colors.white,
      duration: const Duration(seconds: 5),
    );
  }
}
