import 'package:flutter/material.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/sharedpref/auth_preferences.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/auth/login_page.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/home_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.gettingReady();
  }

  void gettingReady() async {
    Future.delayed(Duration(seconds: 4), () {
      AuthPreferences.getUser().then(
        (value) {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (_) => value != null ? HomePage() : LoginPage(),
            ),
          );
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.primary,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              "Pasar Desa",
              style: TextStyle(
                fontSize: 32,
                color: Colors.white,
              ),
            ),
            Text(
              "Sambungrejo",
              style: TextStyle(fontSize: 32, color: Colors.white, height: 0),
            )
          ],
        ),
      ),
    );
  }
}
