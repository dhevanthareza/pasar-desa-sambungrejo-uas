import 'package:flutter/material.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/component/loading.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/transaction_model.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/transaction/transaction_detail_page.dart';

class TransactionHistoryPage extends StatefulWidget {
  const TransactionHistoryPage({Key? key}) : super(key: key);

  @override
  _TransactionHistoryPageState createState() => _TransactionHistoryPageState();
}

class _TransactionHistoryPageState extends State<TransactionHistoryPage> {
  bool isLoading = true;
  List<TransactionModel> transactions = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchTransaction();
  }

  fetchTransaction() async {
    List<dynamic> response = await Request.get('/transaction');
    setState(() {
      this.transactions =
          response.map((e) => TransactionModel.fromJson(e)).toList();
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primary,
        title: Text("History Transaksi"),
      ),
      body: isLoading
          ? Center(
              child: Loading(),
            )
          : Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              child: SingleChildScrollView(
                child: Column(
                    children: transactions
                        .map(
                          (TransactionModel tr) => InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => TransactionDetailPage(
                                    transaction: tr,
                                  ),
                                ),
                              );
                            },
                            child: Card(
                              margin: EdgeInsets.only(bottom: 10),
                              child: Container(
                                width: double.infinity,
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${tr.tanggal}",
                                      style: TextStyle(fontSize: 25),
                                    ),
                                    Text(
                                      "Total Transaksi : ${tr.total}",
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                        .toList()),
              ),
            ),
    );
  }
}
