class ProductModel {
  String? kdBrg;
  String? nmBrg;
  String? satuan;
  String? description;
  int? harga;
  int? hargaBeli;
  int? stok;
  int? stokMin;
  String? gambar;

  ProductModel(
      {this.kdBrg,
      this.nmBrg,
      this.satuan,
      this.harga,
      this.hargaBeli,
      this.stok,
      this.stokMin,
      this.gambar});

  ProductModel.fromJson(Map<String, dynamic> json) {
    kdBrg = json['kd_brg'];
    nmBrg = json['nm_brg'];
    satuan = json['satuan'];
    harga = json['harga'];
    hargaBeli = json['harga_beli'];
    description = json['description'];
    stok = json['stok'];
    stokMin = json['stok_min'];
    gambar = json['gambar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['kd_brg'] = this.kdBrg;
    data['nm_brg'] = this.nmBrg;
    data['satuan'] = this.satuan;
    data['harga'] = this.harga;
    data['harga_beli'] = this.hargaBeli;
    data['stok'] = this.stok;
    data['stok_min'] = this.stokMin;
    data['gambar'] = this.gambar;
    return data;
  }
}
