import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:load/load.dart';
import 'package:pasar_desa_sambungrejo_flutter/api/client.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/transaction_model.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';

class TransactionDetailPage extends StatefulWidget {
  const TransactionDetailPage({Key? key, required this.transaction})
      : super(key: key);
  final TransactionModel transaction;
  @override
  _TransactionDetailPageState createState() => _TransactionDetailPageState();
}

class _TransactionDetailPageState extends State<TransactionDetailPage> {
  bool isPaid = false;
  XFile? file;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      this.isPaid = widget.transaction.statusPembayaran == 1;
    });
  }

  pickImageFromCamera() async {
    XFile? image = await new ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 50);
    setState(() {
      this.file = image;
    });
  }

  pickImageFromGallery() async {
    XFile? image = await new ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 50);
    setState(() {
      this.file = image;
    });
  }

  submitBuktiBayar() async {
    showLoadingDialog(tapDismiss: false);
    Map<String, dynamic> payload = {
      "bukti_bayar": await MultipartFile.fromFile(this.file!.path)
    };
    await Request.post(
      "/transaction/${widget.transaction.id}/bukti-bayar",
      data: FormData.fromMap(payload),
    );
    setState(() {
      isPaid = true;
    });
    hideLoadingDialog();
  }

  createPdf() async {
    final font = await rootBundle.load("assets/fonts/Dongle-Regular.ttf");
    final ttf = pw.Font.ttf(font);
    final pdf = pw.Document();
    pdf.addPage(
      pw.Page(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return pw.Center(
              child: pw.Text("asddas", style: pw.TextStyle(font: ttf)));
        },
      ),
    ); // Page
    final output = await getTemporaryDirectory();
    final file = File("${output.path}/example.pdf");
    await file.writeAsBytes(await pdf.save());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.only(
          right: 25,
          left: 25,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 35,
              ),
              invoiceContent(),
              isPaid
                  ? TextButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.all(5)),
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 0)),
                          backgroundColor:
                              MaterialStateProperty.all(AppColor.primary)),
                      onPressed: () {
                        createPdf();
                      },
                      child: const Text(
                        "Cetak PDF",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    )
                  : uploadBuktiBayar(),
            ],
          ),
        ),
      ),
    );
  }

  Widget invoiceContent() {
    return Column(
      children: [
        Icon(
          isPaid ? Icons.check_circle_outline_outlined : Icons.close_outlined,
          color: isPaid ? Colors.green : Colors.red,
          size: 100,
        ),
        Text(
          "${isPaid ? 'Sudah Terbayar' : 'Belum Terbayar'}",
          style: TextStyle(
            fontSize: 35,
            color: isPaid ? Colors.green : Colors.red,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          "${widget.transaction.total}",
          style: TextStyle(
            fontSize: 45,
            color: AppColor.primary,
            fontWeight: FontWeight.w500,
          ),
        ),
        Container(
          height: 2,
          width: double.infinity,
          color: Color(0xffe2e2e2),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Tanggal",
              style: TextStyle(fontSize: 25),
            ),
            Text("${widget.transaction.tanggal}",
                style: TextStyle(fontSize: 25))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "No nota",
              style: TextStyle(fontSize: 25),
            ),
            Text("${widget.transaction.id}", style: TextStyle(fontSize: 25))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Total Pembelian",
              style: TextStyle(fontSize: 25),
            ),
            Text("${widget.transaction.totalPembelian}",
                style: TextStyle(fontSize: 25))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Ongkir",
              style: TextStyle(fontSize: 25),
            ),
            Text("${widget.transaction.ongkir}", style: TextStyle(fontSize: 25))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Total Transaksi",
              style: TextStyle(fontSize: 25),
            ),
            Text(
              "${widget.transaction.total}",
              style: TextStyle(fontSize: 25),
            )
          ],
        ),
      ],
    );
  }

  dynamic pwInvoiceContent() {
    return pw.Column(
      children: [
        pw.Text(
          "${isPaid ? 'Sudah Terbayar' : 'Belum Terbayar'}",
          style: pw.TextStyle(
            fontSize: 35,
            fontWeight: pw.FontWeight.bold,
          ),
        ),
        // pw.Text(
        //   "${widget.transaction.total}",
        //   style: TextStyle(
        //     fontSize: 45,
        //     color: AppColor.primary,
        //     fontWeight: FontWeight.w500,
        //   ),
        // ),
        // Container(
        //   height: 2,
        //   width: double.infinity,
        //   color: Color(0xffe2e2e2),
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     const Text(
        //       "Tanggal",
        //       style: TextStyle(fontSize: 25),
        //     ),
        //     Text("${widget.transaction.tanggal}",
        //         style: TextStyle(fontSize: 25))
        //   ],
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     const Text(
        //       "No nota",
        //       style: TextStyle(fontSize: 25),
        //     ),
        //     Text("${widget.transaction.id}", style: TextStyle(fontSize: 25))
        //   ],
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     const Text(
        //       "Total Pembelian",
        //       style: TextStyle(fontSize: 25),
        //     ),
        //     Text("${widget.transaction.totalPembelian}",
        //         style: TextStyle(fontSize: 25))
        //   ],
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     const Text(
        //       "Ongkir",
        //       style: TextStyle(fontSize: 25),
        //     ),
        //     Text("${widget.transaction.ongkir}", style: TextStyle(fontSize: 25))
        //   ],
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     const Text(
        //       "Total Transaksi",
        //       style: TextStyle(fontSize: 25),
        //     ),
        //     Text(
        //       "${widget.transaction.total}",
        //       style: TextStyle(fontSize: 25),
        //     )
        //   ],
        // ),
      ],
    );
  }

  Widget uploadBuktiBayar() {
    return Column(
      children: [
        TextButton(
          style: ButtonStyle(
              padding: MaterialStateProperty.all(EdgeInsets.all(5)),
              minimumSize: MaterialStateProperty.all(Size(double.infinity, 0)),
              backgroundColor: MaterialStateProperty.all(AppColor.primary)),
          onPressed: () {
            FocusScope.of(context).requestFocus(new FocusNode());
            showModalBottomSheet(
              context: context,
              builder: (context) => Container(
                child: Wrap(
                  children: [
                    ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Ambil gambar dari galeri'),
                      onTap: () {
                        pickImageFromGallery();
                        Navigator.of(context).pop();
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.photo_camera),
                      title: Text('Ambil gambar dari kamera'),
                      onTap: () {
                        pickImageFromCamera();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            );
          },
          child: const Text(
            "Upload Bukti Bayar",
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
        ),
        this.file != null
            ? Container(
                width: 300,
                height: 170,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: Image.file(
                      File(
                        this.file!.path,
                      ),
                    ).image,
                  ),
                ),
              )
            : SizedBox(),
        this.file != null
            ? TextButton(
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.all(5)),
                    minimumSize:
                        MaterialStateProperty.all(Size(double.infinity, 0)),
                    backgroundColor:
                        MaterialStateProperty.all(AppColor.primary)),
                onPressed: () {
                  submitBuktiBayar();
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: const Text(
                  "Kirim Bukti Bayar",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              )
            : SizedBox(),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
