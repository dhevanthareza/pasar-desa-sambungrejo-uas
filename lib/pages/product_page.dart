import 'package:flutter/material.dart';
import 'package:pasar_desa_sambungrejo_flutter/config.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/model/product_model.dart';

class ProductPage extends StatefulWidget {
  const ProductPage({Key? key, required this.product}) : super(key: key);
  final ProductModel product;
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.network(
                "${AppConfig.gateway}/images/${widget.product.kdBrg}.jpg",
                width: double.infinity,
                height: 350,
                fit: BoxFit.cover,
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.product.nmBrg!,
                      style: TextStyle(fontSize: 35),
                    ),
                    Text(
                      widget.product.description!,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      "Harga : ${widget.product.harga}",
                      style: TextStyle(fontSize: 25, color: AppColor.primary),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
