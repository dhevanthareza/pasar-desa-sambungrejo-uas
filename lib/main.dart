import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:load/load.dart';
import 'package:pasar_desa_sambungrejo_flutter/component/loading.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/constant/app_color.dart';
import 'package:pasar_desa_sambungrejo_flutter/data/provider/cart_controller.dart';
import 'package:pasar_desa_sambungrejo_flutter/pages/splash_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Get.put(CartProvider());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pasar Desa Sambung Rejo',
      theme: ThemeData(
          fontFamily: 'Dongle',
          primarySwatch: Colors.blue,
          primaryColor: AppColor.primary),
      builder: (BuildContext context, dynamic child) {
        return LoadingProvider(
          child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: child),
          loadingWidgetBuilder: (ctx, data) {
            return Center(
              child: Loading(),
            );
          },
          themeData: LoadingThemeData(tapDismiss: false),
        );
      },
      home: const SplashPage(),
    );
  }
}
