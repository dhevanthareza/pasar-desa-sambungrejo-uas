import 'dart:convert';

import 'package:pasar_desa_sambungrejo_flutter/model/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPreferences {
  static const IS_LOGGED_IN = "IS_LOGGED_IN";
  static const USER = "USER";

  static saveAuthPref(UserModel? user) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(USER, jsonEncode(user));
  }

  static Future<UserModel?> getUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var userJson = preferences.getString(USER);
    if (userJson == null) {
      return null;
    }
    var userMap = jsonDecode(userJson);
    UserModel user = UserModel.fromJson(userMap);
    return user;
  }
}